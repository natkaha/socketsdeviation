import flask
import logging
from flask_restful import reqparse



app = flask.Flask(__name__)
app.url_map.strict_slashes = False
logging.basicConfig(filename="deviations.log", level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')


@app.route('/')
def root():
    return flask.jsonify({'message': 'Ok'})


@app.route('/start_check/')
def start_check():
    r'''
    Endpoint для начала классификации
    :return:
    :rtype json
    '''
    from calculate_deviations import deviation
    logging.info("=========Start check=========")

    logging.info("Fetch input path")
    parser = reqparse.RequestParser()
    parser.add_argument(name='path', required=True, type=str, location='args')
    args = parser.parse_args()
    # TODO
    logging.info(f"Input path is: {args.path}/")


    success, result  = deviation(args.path)
    return flask.jsonify({'success': success, 'result': result})


if __name__ == '__main__':
    app.run(port=2000)
