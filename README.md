# SocketsDeviation
This is the repo of project for sockets deviation estimation by image.
The code is on Python 3, all the requirements for dependencies could be found in requirements.txt.

To make this project work after installation of libraries you need to run flask_app script by following command:
`python3 flask_app.py`

The only argument is *path*, to test the algorithm pass *path=inputpart * using API.
Thus API is following:
`http://127.0.0.1:2000/start_check/?path=inputpart`

The log is written to deviation.log file.

**NOTE**
The input images must contain the chessboard, otherway the precision of algorithm is not guaranteed.
