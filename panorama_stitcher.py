"""
Stitching a panorama
================

Stitcher API for python to stitch panoramas.
"""


import os
import cv2
import glob
import logging

tmp_path = "tmp"

def rotate_image(mat, angle):
    """
    Rotates an image (angle in degrees) and expands image to avoid cropping
    """

    height, width = mat.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_mat[0,0]) 
    abs_sin = abs(rotation_mat[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_mat[0, 2] += bound_w/2 - image_center[0]
    rotation_mat[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation matrix
    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat

def read_input_images(path):
    r'''
    Считывает входные данные
    :param path: Путь к файлу
    :type path: str
    :return: Результат считывания
    '''
    imgs = []
    data_path = os.path.join(path, '*jpg')
    files = glob.glob(data_path)
    for img_path in files:
        logging.info(f"Process file {img_path}")
        img_ = cv2.imread(img_path)
        if img_ is None:
            return False, [], f"Can't read image {img_path}"
        img_rotated = rotate_image(img_, 90)
        imgs.append(img_rotated)
    return True, imgs, "Ok"


def get_panorama(path):
    modes = (cv2.Stitcher_PANORAMA, cv2.Stitcher_SCANS)

    num_try = 0
    while num_try < 5:
        result, imgs, msg = read_input_images(path) # read input images
        if not result:
            return None, False, msg
        stitcher = cv2.createStitcher(True)
        status, pano = stitcher.stitch(imgs)
        if status == cv2.Stitcher_OK:
            break
        elif status==3:
            logging.info(f"Can't stitch images, error code = {status}, try {num_try} out of 5")
            num_try += 1
        else:
            return None, False, f"Can't stitch images, error code = {status}"
    rotated_pano = rotate_image(pano, -90)   
    cv2.imwrite(os.path.join(tmp_path, 'output_pano.jpg'), rotated_pano)
    logging.info("Stitching completed successfully.")

    return rotated_pano, True, msg
