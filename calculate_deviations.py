'''

This is main module for sockets deviation calculation and class recognition.
'''
import os
import cv2
import json
import logging
import shutil
import numpy as np

from skimage import img_as_ubyte
from skimage.io import imread
from skimage.color import rgb2gray
from skimage.feature import register_translation
from skimage.transform import rotate, AffineTransform, warp
from panorama_stitcher import get_panorama

logging.basicConfig(filename="deviations.log", level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')

# density = 2.9

templates_path = "templates"
tmp_path = "tmp"

def get_roi(num_interest, centers_pattern, shifted, reg=20):
    r'''

    :param num_interest:
    :type num_interest: int
    :param reg:

    :return:
    :rtype: list
    '''
    roi = []
    for i in range(num_interest):
        row = centers_pattern[i][1]
        col = centers_pattern[i][0]
        roi.append(shifted[row - reg:row + reg, col - reg:col + reg])
    return roi


def get_params(minArea=25.0,  minCircularity=0.8, minConvexity=0.95, minInertiaRatio=0.1,
               filterByCircularity=False, filterByConvexity=True, filterByInertia=True, filterByArea=True):
    r'''

    :param minArea:
    :type minArea: float
    :param minCircularity:
    :type minCircularity: float
    :param minConvexity:
    :type minConvexity: float
    :param minInertiaRatio:
    :type minInertiaRatio: float
    :param filterByCircularity:
    :type filterByCircularity: bool
    :param filterByConvexity:
    :type filterByConvexity: bool
    :param filterByInertia:
    :type filterByInertia: bool
    :param filterByArea:
    :type filterByArea: bool
    :return: params
    :rtype:
    '''

    params = cv2.SimpleBlobDetector_Params()
    params.filterByArea = filterByArea
    params.minArea = minArea  # The dot in 20pt font has area of about 30
    params.filterByCircularity = filterByCircularity
    params.minCircularity = minCircularity
    params.filterByConvexity = filterByConvexity
    params.minConvexity = minConvexity
    params.filterByInertia = filterByInertia
    params.minInertiaRatio = minInertiaRatio
    return params

def density_calculation(rotated_pano):
    imgpoints = [] # 2d points in image plane.
    img = rotated_pano.copy()
    ret, corners = cv2.findChessboardCorners(rotated_pano, (3,3),None)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # If found, add object points, image points (after refining them)
    density = 2.88
    if ret == True:
        # corners2 = cv2.cornerSubPix(gray,corners,(3,3),(-1,-1),criteria)
        imgpoints.append(corners)
        logging.info('Chessboard is found')
        imgpoints=np.squeeze(imgpoints)
        sum = 0
        cou = 0
        eps = 0.1
        chess_cell_mm  = 10
        for i in range(1,len(imgpoints)):
            if abs(imgpoints[i][1]-imgpoints[i-1][1]) < eps:
                cou+=1
                sum+=abs(imgpoints[i][0]-imgpoints[i-1][0])
            # else:
                # logging.info(f"The {abs(imgpoints[i][1]-imgpoints[i-1][1])}")
        density = sum/cou/chess_cell_mm      
        logging.info(f"The density of mm is {density} pixels")
    return density, ret    

def remove_tmp():
    folder = tmp_path
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            logging.info(e)
def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized


def deviation(path):
    low_threshold = 15
    ratio = 3
    kernel_size = 500
    remove_tmp()
    path = os.path.join("C:/", path)
    src, ret, msg = get_panorama(path)

    if not ret:
        logging.info(msg)
        return False, {}
    
    density, ret = density_calculation(src)
    if not ret:
        logging.info("Chessboard is not found, density is taken average = 2.88, the result could be not precise")
        
    src_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.blur(src_gray, (15, 15))

    detected_edges = 255 - cv2.Canny(img_blur, low_threshold, low_threshold * ratio, kernel_size)

    edges_blur = cv2.blur(detected_edges, (3, 3))

    cv2.imwrite(os.path.join(templates_path,'edges.png'), edges_blur)
    image = img_as_ubyte(rgb2gray(imread(os.path.join(templates_path,'edges.png'))))
    image_source = img_as_ubyte(rgb2gray(imread(os.path.join(templates_path,'sized__.png'))))
    moving_source = img_as_ubyte(rgb2gray(imread(os.path.join(templates_path,'BORD.png'))))
    # shift = (0, 0)
    image = image_resize(image,width = 1000)
    moving_source = cv2.resize(moving_source, (image.shape[1], image.shape[0]), interpolation = cv2.INTER_AREA)
    image_source = cv2.resize(image_source, (image.shape[1], image.shape[0]), interpolation = cv2.INTER_AREA)
    res = []
    for i in range(0, 1): #(-10, 10, 2)
        logging.info(i / 10.0)
        moving = rotate(moving_source, i / 10.0, preserve_range=True, mode='constant', cval=255)
        shift, error, diffphase = register_translation(image, moving)
        res.append([i, shift, error])
        logging.info(f"{shift}, {error}, {diffphase}")

    temp = 1
    current = 0
    for i in range(len(res)):
        if res[i][2] < temp:
            temp = res[i][2]
            current = i
    logging.info(f'Best res: {res[current]}')
    best = res[current]

    moving_best = rotate(moving_source, best[0] / 10.0, preserve_range=True, mode='constant', cval=255)
    moving_best = moving_best.astype('uint8')

    template = rotate(image_source, best[0] / 10.0, preserve_range=True, mode='constant', cval=255)
    template = template.astype('uint8')

    shift, error, diffphase = register_translation(image, moving_best)

    transform = AffineTransform(translation=(shift[1], shift[0]))
    shifted = warp(template, transform, mode='wrap', preserve_range=False) * 255
    template = template.astype('uint8') # HERE SHOULD BE template = shifted.astype('uint8')
    template = cv2.resize(template, (src_gray.shape[1], src_gray.shape[0]), interpolation=cv2.INTER_AREA)
    added_image = cv2.addWeighted(src_gray, 0.5, template, 0.5, 0)
    cv2.imwrite(os.path.join(tmp_path,'overlay.png'), added_image)

    # Read image
    h_res = src.copy()
    # template = cv2.resize(template,(src.shape[1],src.shape[0])) #COULD CAUSE A BUG

    im = template

    # Set up the detector with default parameters.
    # Perform a simple blob detect
    font = cv2.FONT_HERSHEY_SIMPLEX
    params = get_params(200, 0.7, 0.8, 0.4, filterByCircularity=True)

    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs.
    keypoints = detector.detect(im)

    num = len(keypoints)

    centers_pattern = list()
    for i in range(num):
        centers_pattern.append((int(keypoints[i].pt[0]), int(keypoints[i].pt[1])))

    num_interest = len(centers_pattern)

    # Draw detected blobs as red circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
    im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    for i in range(num_interest):
        im_with_keypoints = cv2.drawMarker(im_with_keypoints, centers_pattern[i], color=(0, 255, 0), markerSize=10, thickness=2)
        im_with_keypoints = cv2.putText(im_with_keypoints, str(i), centers_pattern[i], font, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
    # Show keypoints
    
#     cv2.imwrite('blobs_pattern.png', im_with_keypoints)

    result = np.zeros(shape=(num_interest,4))
    reg = 70
    region_for_internal = 11
    roi = get_roi(num_interest, centers_pattern, h_res, reg)
    roi_true = roi.copy()
    roi_internal = []

    params = get_params(minArea=300, filterByArea=True)
    k = 0
    detector2 = cv2.SimpleBlobDetector_create(params)

    for i in range(num_interest):
  
        roi[i] = cv2.convertScaleAbs(roi[i], alpha=1.4, beta=0)  
        roi[i] = cv2.medianBlur(roi[i],13)  
  
        img = roi[i]
        Z = img.reshape((-1,1))
        Z = np.float32(Z)

        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        K = 7
        ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
        # Now convert back into uint8, and make original image
        center = np.uint8(center)
        res = center[label.flatten()]
        roi[i] = res.reshape((img.shape))
        
        keypoints = detector2.detect(roi[i])
        if (len(keypoints)!=0): 
            k+=1
            center_row = int(keypoints[0].pt[1])
            center_col = int(keypoints[0].pt[0])
            roi_internal.append(roi_true[i][center_row - region_for_internal:center_row + region_for_internal,
                                center_col - region_for_internal:center_col + region_for_internal])
            img_blur = roi_internal[len(roi_internal)-1]
            detected_edges = 255 - cv2.Canny(img_blur, 50, 100, kernel_size)
            img = detected_edges
            # cimg = cv2.cvtColor(detected_edges, cv2.COLOR_GRAY2BGR)

            circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 5,
                                       param1=10, param2=15, minRadius=5, maxRadius=0)
            if circles is not None:
                circles = circles[0]
                inner_diameter = 2 * max(circles[:,2]) / density
            else:
                inner_diameter = 0
            deviation = np.sqrt(np.square(center_col - reg) + np.square(center_row - reg))
            result[i][0] = i
            result[i][1] = (keypoints[0].pt[0] - reg) / density
            result[i][2] = (keypoints[0].pt[1] - reg) / density
            outer_diameter = keypoints[0].size/density
            
            if (outer_diameter < 10.5) and (outer_diameter > 7.5):
                result[i][3] = 20
            elif outer_diameter < 12.5:
                if inner_diameter < 6.5:
                    result[i][3] = 21
                else:
                    result[i][3] = 23
            elif outer_diameter < 16.5:
                result[i][3] = 26
            else:
                result[i][3] = 0

            logging.info("For {} socket deviation is: {} mm, the outer diameter is {} pxls".format(i,deviation/density,keypoints[0].size, ))
        roi[i] = cv2.drawKeypoints(roi[i], keypoints, np.array([]), (255,0,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        cv2.imwrite(os.path.join(tmp_path, f"{i}.jpg"),roi[i])
    logging.info(f"Amount of sockets is {k}")
    logging.info(result)
    to_dict = {}
    for res in result:
        to_dict[str(int(res[0]))]={"x_diff": res[1], "y_diff": res[2], "socket_class": int(res[3])}
    result_json = json.dumps(to_dict)    
    print(result_json)

    logging.info("=========Calculation is finished=========")
    return True, result_json

if __name__ == '__main__':
    pass
